'''
                                  ESP Health
                           Combined Hepatitis A/B/C
                              Disease Definition


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
#
# This may not still be true in newer versions of Django - JM 6 Dec 2011
from collections import defaultdict
from decimal import Decimal

from dateutil.relativedelta import relativedelta
from django.db import IntegrityError
from django.db.models import F
from django.db.models import Max

from ESP.hef.base import AbstractLabTest
from ESP.hef.base import BaseEventHeuristic
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import LabResultFixedThresholdHeuristic
from ESP.hef.base import LabResultPositiveHeuristic
from ESP.hef.base import LabResultRatioHeuristic
from ESP.hef.models import Event
from ESP.nodis.base import DiseaseDefinition
from ESP.utils import log


class PotentialCase(object):
    def __init__(self, date, criteria, event, events):
        self.date = date
        self.criteria = criteria
        self.event = event
        self.events = events


class PotentialCaseList(object):
    def __init__(self, patient):
        self.patient = patient
        self.cases = []

    def add_case(self, case):
        self.cases.append(case)

    def get_earliest_case(self):
        self.cases.sort(key=lambda phc: phc.date)
        return self.cases[0]

    def gather_events(self):
        ev = []
        for case in self.cases:
            ev.extend(case.events)
        return set(ev)


class Hepatitis_A(DiseaseDefinition):
    '''
    Hepatitis A
    '''

    # A future version of this disease definition may also detect chronic hep a
    conditions = ['hepatitis_a:acute']

    uri = 'urn:x-esphealth:disease:channing:hepatitis-combined:hepatitis_a:v1'

    short_name = 'hepatitis_a:acute'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans'
    ]

    timespan_heuristics = []

    @property
    def event_heuristics(self):
        """
        Event heuristics used by all Hepatitis variants
        """
        heuristic_list = []
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_a_igm_antibody',
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='ast',
            ratio=2,
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='alt',
            ratio=2,
        ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        #
        # Acute Hepatitis A
        #
        # (dx:jaundice or lx:alt:ratio:2.0 or lx:ast:ratio:2.0) 
        # AND lx:hepatitis_a_igm_antibody:positive within 30 (changed from 30) days
        #
        primary_event_name = 'lx:hepatitis_a_igm_antibody:positive'
        secondary_event_names = [
            'dx:jaundice',
            'lx:alt:ratio:2',
            'lx:ast:ratio:2',
        ]
        event_qs = Event.objects.filter(
            name=primary_event_name,
            patient__event__name__in=secondary_event_names,
            patient__event__date__gte=(F('date') - 14),
            patient__event__date__lte=(F('date') + 14),
        )
        relevant_event_names = [primary_event_name] + secondary_event_names
        counter = self._create_cases_from_event_qs(
            condition='hepatitis_a:acute',
            criteria='Criteria #1: [(jaundice (not of newborn) or (alt or ast) > 2x ULN) and positive of hep_a_igm] w/in 14 days',
            recurrence_interval=None,
            event_qs=event_qs,
            relevant_event_names=relevant_event_names,
        )
        log.debug('Created %s new Hep A ' % counter)
        return counter


class Hepatitis_B(DiseaseDefinition):
    '''
    Hepatitis B
    '''

    # A future version of this disease definition may also detect chronic hep a
    conditions = ['hepatitis_b:acute']

    uri = 'urn:x-esphealth:disease:channing:hepatitis-combined:hepatitis_b:v1'

    short_name = 'hepatitis_b:acute'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans'
    ]

    timespan_heuristics = []

    @property
    def event_heuristics(self):
        '''
        Event heuristics used by all Hepatitis variants
        '''
        heuristic_list = []
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='bilirubin_total',
            threshold=Decimal('1.5'),
            match_type='gt',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_surface_antigen',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_viral_dna',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='bilirubin_direct',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='bilirubin_indirect',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_core_antigen_igm_antibody',
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='alt',
            ratio=5,
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='ast',
            ratio=5,
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_b:chronic',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B18.1', type='icd10'),
                Dx_CodeQuery(starts_with='B18.0', type='icd10'),
                Dx_CodeQuery(starts_with='070.32', type='icd9'),
            ]
        ))

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        counter = 0
        counter += self.generate_definition_a()
        counter += self.generate_definition_b_c()
        counter += self.generate_definition_d()

        return counter

    def generate_definition_a(self):
        '''
        a) (#1 or #2 or #3) AND #4 within 14 day period
        1. ICD9 = 782.4 (jaundice, not of newborn)
        2. Alanine aminotransferase (ALT) >5x upper limit of normal
        3. Aspartate aminotransferase (AST) >5x upper limit of normal
        4. IgM antibody to Hepatitis B Core Antigen = "REACTIVE" (may be truncated)
        '''
        log.info('Generating cases for Hepatitis B definition A')
        trigger_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_core_antigen_igm_antibody:positive')
        trigger_qs = trigger_qs.exclude(case__condition=self.conditions[0])
        trigger_qs = trigger_qs.order_by('date')
        confirmation_qs = BaseEventHeuristic.get_events_by_name(name='dx:jaundice')
        confirmation_qs |= BaseEventHeuristic.get_events_by_name(name='lx:alt:ratio:5')
        confirmation_qs |= BaseEventHeuristic.get_events_by_name(name='lx:ast:ratio:5')
        confirmation_qs = confirmation_qs.order_by('date')
        counter = 0
        for trigger_event in trigger_qs:
            pat = trigger_event.patient
            begin_relevancy = trigger_event.date - relativedelta(days=14)
            end_relevancy = trigger_event.date + relativedelta(days=14)
            pat_conf_qs = confirmation_qs.filter(
                patient=pat,
                date__gte=begin_relevancy,
                date__lte=end_relevancy,
            )
            if not pat_conf_qs:
                # This patient does not have Hep B
                continue
            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria #1: [positive Hep B core antigen igm antibody and (jaundice (not of newborn) or ast>5x ULN or alt >5x ULN)] w/in 14 days',
                recurrence_interval=None,  # Does not recur
                event_obj=trigger_event,
                relevant_event_qs=pat_conf_qs,
            )
            if created:
                counter += 1
        log.debug('Created %s new Hep B definition a cases' % counter)
        return counter

    def generate_definition_b_c(self):
        '''
        b) (#1 or #2 or #3) AND (#12 or #15) AND #5 "reactive" within 21 day period 
            AND no prior positive result for #5 or #7 ever 
            AND no code for chronic hep b ICD9=070.32 at this encounter or in patient's past
        c) (#1 or #2 or #3) AND (#12 or #15) AND #7  positive within 21 day period 
            AND no prior positive result for #5 or #7 ever 
            AND no code for chronic hep b ICD9=070.32 at this encounter or in the patient's past
        1. ICD9 = 782.4 (jaundice, not of newborn)
        2. Alanine aminotransferase (ALT) >5x upper limit of normal
        3. Aspartate aminotransferase (AST) >5x upper limit of normal
        5. Hepatitis B Surface Antigen
        7. Hepatitis B Viral DNA
        12. Total bilirubin > 1.5
        15. Calculated bilirubin = (direct bilirubin + indirect bilirubin) = value > 1.5
        ICD9 070.32 = Chronic Hep B
        '''
        log.info('Generating cases for Hepatitis B definition B')
        counter = 0
        #
        # Surface antigen test and viral DNA test are the trigger events (#5 or #7)
        #
        hep_b_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:positive')
        hep_b_pos_qs |= BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_viral_dna:positive')
        #
        # Unbound positives are trigger events
        #
        trigger_qs = hep_b_pos_qs.exclude(case__condition=self.conditions[0])
        trigger_qs = trigger_qs.order_by('date')
        #
        # Chronic Hep B diagnosis
        #
        chronic_hep_b_qs = BaseEventHeuristic.get_events_by_name(name='dx:hepatitis_b:chronic')
        #
        # Jaundice and associated high ALT/AST results (#1 or #2 or #3)
        #
        jaundice_qs = BaseEventHeuristic.get_events_by_name(name='dx:jaundice')
        jaundice_qs |= BaseEventHeuristic.get_events_by_name(name='lx:alt:ratio:5')
        jaundice_qs |= BaseEventHeuristic.get_events_by_name(name='lx:ast:ratio:5')
        #
        # Bilirubin - total bilirubin events can be queries directly; but 
        # calculated bilirubin must be, ahem, calculated programmatically.
        #
        total_bili_qs = BaseEventHeuristic.get_events_by_name(name='lx:bilirubin_total:threshold:gt:1.5')
        direct_bili_labs_qs = AbstractLabTest('bilirubin_direct').lab_results
        indirect_bili_labs_qs = AbstractLabTest('bilirubin_indirect').lab_results

        for trigger_event in trigger_qs:
            patient = trigger_event.patient
            date = trigger_event.date
            relevancy_start = date - relativedelta(days=21)
            relevancy_end = date + relativedelta(days=21)
            #
            # No chronic hep B diagnosis
            #
            if chronic_hep_b_qs.filter(patient=patient, date__lte=date):
                continue  # Patient has Chronic Hep B
            #
            # No prior Hep B test positive
            #
            if hep_b_pos_qs.filter(patient=patient, date__lt=date):
                continue  # Patient has Chronic Hep B
            #
            # Patient must have jaundice or high ALT/AST results
            #
            pat_jaundice_qs = jaundice_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            if not pat_jaundice_qs:
                continue  # Patient does not have Hep B
            #
            # Patient must have elevated bilirubin values.  We check the total
            # bilirubin event first since it's a simple query.  If it is not 
            # found, we check calculated bilirubin.
            #
            pat_total_bili_qs = total_bili_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            pat_direct_bili_qs = direct_bili_labs_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            pat_indirect_bili_qs = indirect_bili_labs_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            either_bili_qs = direct_bili_labs_qs | indirect_bili_labs_qs
            either_bili_qs = either_bili_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            ).order_by('date')
            if pat_total_bili_qs:
                criteria = 'total bilirubin>1.5'
            else:
                criteria = 'calculated bilirubin >1.5'
                calculated_bilirubin = 0
                bili_date_list = either_bili_qs.values_list('date', flat=True)
                for bili_date in bili_date_list:
                    max_direct_dict = pat_direct_bili_qs.filter(date=bili_date).aggregate(Max('result_float'))
                    if max_direct_dict['result_float__max']:
                        max_direct = max_direct_dict['result_float__max']
                    else:
                        max_direct = 0
                    max_indirect_dict = pat_indirect_bili_qs.filter(date=bili_date).aggregate(Max('result_float'))
                    if max_indirect_dict['result_float__max']:
                        max_indirect = max_indirect_dict['result_float__max']
                    else:
                        max_indirect = 0
                    total_this_date = max_direct + max_indirect
                    if total_this_date > calculated_bilirubin:
                        calculated_bilirubin = total_this_date
                if not calculated_bilirubin > 1.5:
                    continue  # Patient does not have Hep B

            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria #2/3: [(jaundice (not of newborn) or alt>5x ULN or ast >5x ULN) and (positive of hep_b_surface or positive of hep_b_viral_dna) and (%s) ]  w/in 21 days;  exclude if: prior/current dx=chronic hepatitis B or prior positive hep_b_surface ever or prior positive hep_b_viral_dna ever' % criteria,
                recurrence_interval=None,  # Does not recur
                event_obj=trigger_event,
                relevant_event_qs=jaundice_qs | total_bili_qs
            )
            if created:
                counter += 1
        log.debug('Created %s new Hep B cases def b/c' % counter)
        return counter

    def generate_definition_d(self):
        '''
        d) #5 "reactive" with record of #5 "non-reactive" within the prior 12 months 
	        AND no prior positive test for #5 or #7 ever 
	        AND no code for ICD9=070.32 at this encounter or in patient's past.  
        Please use "date collected" (or if unavailable then "date ordered") for 
        comparison of dates.
        5. Hepatitis B Surface Antigen = "REACTIVE" (may be truncated)
        7. Hepatitis B Viral DNA
        '''
        log.info('Generating cases for Hep B definition D')
        counter = 0
        surface_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:positive')
        surface_neg_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:negative')
        viral_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_viral_dna:positive')
        chronic_dx_qs = BaseEventHeuristic.get_events_by_name(name='dx:hepatitis_b:chronic')
        unbound_surface_pos_qs = surface_pos_qs.exclude(case__condition__in=self.conditions)
        #
        # Patient must have a positive Hep B surface antigen test
        #
        for surface_pos_event in unbound_surface_pos_qs.order_by('date'):
            #
            # Patient must have a negative surface antigen result in the past 12 months
            #
            relevancy_begin = surface_pos_event.date - relativedelta(months=12)
            prior_neg_qs = surface_neg_qs.filter(patient=surface_pos_event.patient)
            prior_neg_qs = prior_neg_qs.filter(date__lt=surface_pos_event.date)
            prior_neg_qs = prior_neg_qs.filter(date__gte=relevancy_begin)
            if not prior_neg_qs:
                # Patient could possibly have Hep B, but existing EMR data is 
                # not sufficient to confirm a case.
                continue
                #
            # Exclude patient if they have a prior surface antigen or viral dna test
            #
            prior_pos_qs = surface_pos_qs | viral_pos_qs
            prior_pos_qs = prior_pos_qs.filter(patient=surface_pos_event.patient)
            prior_pos_qs = prior_pos_qs.filter(date__lt=surface_pos_event.date)
            if prior_pos_qs:
                continue  # Patient does not have acute Hep B - probably has chronic
            #
            # Exclude patient if they have a chronic hep b diagnosis at this time or earlier.
            #
            if chronic_dx_qs.filter(patient=surface_pos_event.patient, date__lte=surface_pos_event.date):
                continue  # Patient has chronic hep b
            #
            # Patient has acute hep b!
            #
            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria #4: positive hep b surface and prior neg of hep b surface w/in 12 months; exclude if:  prior/current dx=chronic hepatitis B or prior positive hep_b_surface ever or prior positive hep_b_viral_dna ever',
                recurrence_interval=None,  # Does not recur
                event_obj=surface_pos_event,
                relevant_event_qs=prior_neg_qs,
            )
            if created:
                counter += 1
        log.debug('Created %s new Hep B def d' % counter)
        return counter


class Hepatitis_C(DiseaseDefinition):
    '''
    Hepatitis C
    '''
    # A future version of this disease definition may also detect chronic hep c
    conditions = ['hepatitis_c:acute']

    uri = 'urn:x-esphealth:disease:channing:hepatitis-combined:hepatitis_c:v1'

    short_name = 'hepatitis_c:acute'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans'
    ]

    potential_case_klass = PotentialCase
    potential_case_list_klass = PotentialCaseList

    def __init__(self, *args, **kwargs):
        super(Hepatitis_C, self).__init__(*args, **kwargs)
        self.potential_cases = {}

    @property
    def event_heuristics(self):
        '''
        Event heuristics used by all Hepatitis variants
        '''
        heuristic_list = []
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_elisa',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_signal_cutoff',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_riba',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_rna',
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:chronic',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B18.2', type='icd10'),
                Dx_CodeQuery(starts_with='070.54', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:unspecified',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B19.20', type='icd10'),
                Dx_CodeQuery(starts_with='070.70', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='alt',
            threshold=Decimal('200'),
            match_type='gt',
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='bilirubin_total',
            threshold=Decimal('1.5'),
            match_type='gt',
        ))

        return heuristic_list

    def generate_hepc(self):
        self._condition_a()
        self._condition_b()
        self._condition_c()
        self._condition_d()

        return self._process_potential_cases()

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        counter = self.generate_hepc()
        log.info('Generated %s new cases for %s (%s)' % (counter, self.short_name, self.uri))
        return counter

    def _condition_ab(self, required_names, negating_names, validating_names, invalidating_names, criteria):
        required_events = self._get_event_values(required_names)
        patient_events = self._build_patient_event_dict(required_events)
        self._update_event_dict_for_existing_cases(patient_events)
        self._remove_negated_events(negating_names, patient_events, validating_names, 28)

        for patient, events in patient_events.items():
            potential_validating = Event.objects.filter(patient=patient, name__in=validating_names)
            potential_negating = Event.objects.filter(patient=patient, name__in=invalidating_names)
            for event in events:
                start = event['date'] - relativedelta(days=28)
                end = event['date'] + relativedelta(days=28)
                validating_events = list(
                    potential_validating.filter(date__gte=start, date__lte=end).values_list('pk', flat=True))
                has_negating_events = potential_negating.filter(date__gte=start, date__lte=end).count() > 0

                if len(validating_events) > 0 and not has_negating_events:
                    self.populate_potential_cases(patient, self.potential_case_klass(event['date'], criteria, event,
                                                                         validating_events + [e['pk'] for e in events]))
                    continue

    def _condition_cd(self, required_names, negating_names, validating_names, criteria):
        required_events = self._get_event_values(required_names)
        patient_events = self._build_patient_event_dict(required_events)
        self._update_event_dict_for_existing_cases(patient_events)
        self._remove_negated_events(negating_names, patient_events)

        for patient, events in patient_events.items():
            potential_validating = Event.objects.filter(patient=patient, name__in=validating_names)
            for event in events:
                start = event['date'] - relativedelta(years=1)
                end = event['date']
                validating_events = list(
                    potential_validating.filter(date__gte=start, date__lt=end).values_list('pk', flat=True))

                if len(validating_events) > 0:
                    self.populate_potential_cases(patient, self.potential_case_klass(event['date'], criteria, event,
                                                                         validating_events + [e['pk'] for e in events]))
                    continue

    def _condition_a(self):
        required_names = ['dx:jaundice',
                          'lx:alt:threshold:gt:200',
                          'lx:bilirubin_total:threshold:gt:1.5']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_elisa:positive']

        invalidating_names = ['lx:hepatitis_c_riba:negative',
                              'lx:hepatitis_c_riba:indeterminate',
                              'lx:hepatitis_c_rna:negative',
                              'lx:hepatitis_c_rna:indeterminate',
                              'lx:hepatitis_c_signal_cutoff:negative',
                              'lx:hepatitis_c_signal_cutoff:indeterminate']

        self._condition_ab(required_names, negating_names, validating_names, invalidating_names, 'Criteria a')

    def _condition_b(self):
        required_names = ['dx:jaundice',
                          'lx:alt:threshold:gt:200',
                          'lx:bilirubin_total:threshold:gt:1.5']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:positive']

        invalidating_names = ['lx:hepatitis_c_riba:negative',
                              'lx:hepatitis_c_riba:indeterminate',
                              'lx:hepatitis_c_signal_cutoff:negative',
                              'lx:hepatitis_c_signal_cutoff:indeterminate']

        self._condition_ab(required_names, negating_names, validating_names, invalidating_names, 'Criteria b')

    def _condition_c(self):
        required_names = ['lx:hepatitis_c_rna:positive']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:negative',
                            'lx:hepatitis_c_elisa:negative']

        self._condition_cd(required_names, negating_names, validating_names, 'Criteria c')

    def _condition_d(self):
        required_names = ['lx:hepatitis_c_elisa:positive']

        negating_names = ['lx:hepatitis_c_riba:positive',
                          'lx:hepatitis_c_rna:positive',
                          'lx:hepatitis_c_elisa:positive',
                          'dx:hepatitis_c:chronic',
                          'dx:hepatitis_c:unspecified']

        validating_names = ['lx:hepatitis_c_rna:negative',
                            'lx:hepatitis_c_elisa:negative']

        self._condition_cd(required_names, negating_names, validating_names, 'Criteria d')

    def _remove_negated_events(self, negating_names, patient_events, offset_names=None, offset=0):
        negating_events = Event.objects.filter(
            name__in=negating_names
        ).order_by('date').values('name', 'patient', 'date')
        patient_negating_events = defaultdict(list)
        if offset_names is None:
            offset_names = []
        for ne in negating_events:
            date = ne['date']
            if ne['name'] in offset_names:
                date = date + relativedelta(days=offset)
            patient_negating_events[ne['patient']].append(date)
        """
        Remove trigger events that are after negating events. If a patient has no more triggering events the remove them
        from the patient_events dict.
        """
        for patient, negate_dates in patient_negating_events.items():
            negate_dates.sort()
            events = patient_events.get(patient, None)
            if events is None:
                continue
            potentials = []
            for event in events:
                if event['date'] <= negate_dates[0]:
                    potentials.append(event)

            if potentials:
                patient_events[patient] = potentials
            else:
                patient_events.pop(patient, None)

    def _get_event_values(self, required_names):
        return Event.objects.filter(
            name__in=required_names
        ).exclude(case__condition=self.conditions[0]).order_by('date').values('pk', 'name', 'patient', 'date')

    def _build_patient_event_dict(self, events):
        patient_events = defaultdict(list)
        for ev in events:
            patient_events[ev['patient']].append(ev)

        return patient_events

    def _update_event_dict_for_existing_cases(self, events):
        patients_with_existing_cases = self.add_events_to_existing_cases(events.keys(),
                                                                         [self._extract_pks(events)])
        pt_ids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        for pid in pt_ids_for_existing_cases:
            events.pop(pid, None)

    def _extract_pks(self, patient_event_dict):
        return {patient: [event['pk'] for event in events] for patient, events in patient_event_dict.items()}

    def populate_potential_cases(self, patient, potential_case):
        self.potential_cases[patient] = self.potential_cases.get(patient, self.potential_case_list_klass(patient))
        self.potential_cases[patient].add_case(potential_case)

    def _process_potential_cases(self):
        new_cases = 0
        for patient, potential_list in self.potential_cases.items():
            first_case = potential_list.get_earliest_case()
            try:
                log.debug('Good Case for %s' % patient)
                t, new_case = self._create_case_from_event_list(
                    condition=self.conditions[0],
                    criteria=first_case.criteria,
                    recurrence_interval=None,  # Does not recur
                    event_obj=Event.objects.get(pk=first_case.event['pk']),
                    relevant_event_names=potential_list.gather_events() - {first_case.event['pk']},
                )
            except IntegrityError:
                log.debug('Case creation failed for     %s' % patient)
                pass  # _create_case_from_event_list only knows how to create a case and return True.
            if t:
                new_cases += 1
                log.info('Created new hepc case %s : %s' % (first_case.criteria, new_case))

        return new_cases


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Packaging
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def event_heuristics():
    return [Hepatitis_A().event_heuristics, Hepatitis_B().event_heuristics, Hepatitis_C().event_heuristics]


def disease_definitions():
    return [Hepatitis_A(), Hepatitis_B(), Hepatitis_C()]
